export default class Component {
  createElement(tag, attr, content = '') {
    const element = document.createElement(tag)
    for (let [key, value] of Object.entries(attr)) {
      element[key] = value
    }

    if (content) {
      element.textContent = content
    }

    return element
  }
}
