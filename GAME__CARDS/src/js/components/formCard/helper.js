export const fields = [
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'role',
    errorText: 'Корректно введите данные',
    placeholder: 'Role'
  },
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'attackValue',
    errorText: 'Корректно введите данные',
    placeholder: 'Attack'
  },
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'name',
    errorText: 'Корректно введите данные',
    placeholder: 'Name hero'
  },
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'hpValue',
    errorText: 'Корректно введите данные',
    placeholder: 'HP'
  },
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'spell',
    errorText: 'Корректно введите данные',
    placeholder: 'Spell'
  }
]
