import Form from '../../common/Form'
import Input from '../../common/Input'
import { fields } from './helper'

export default class FormSignUp extends Form {
  constructor(data) {
    super(data)
    this.data = data
  }
  request() {
    // TODO post request to server
  }

  render() {
    const form = super.render(this.data)

    fields.forEach(field => {
      form.append(new Input(field).render())
    })

    return form
  }
}
