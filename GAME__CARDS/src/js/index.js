import './components/Card'
import FormCard from './components/formCard/FormCard'
import FormSignIn from './components/formSignIn/FormSignIn'
import FormSignUp from './components/formSignUp/FormSignUp'
import Modal from './components/Modal'

// ******* EXAMPLE ******* //
// function User(name){
//     this.name = name
// }

// new User('andy')

// class User {
//     constructor(name) {
//         this.name = name
//     }
// }
// ******* EXAMPLE ******* //

const openModalCard = document.getElementById('create')

openModalCard.addEventListener('click', () => {
  const formCard = new FormCard({ id: 'formCard' })

  const wrapper = document.createElement('div')
  wrapper.innerHTML = `<div class="card">
        <div class="card__border"></div>
        <h2 class="card__role " id="role"></h2>
        <div class="card__3dmodel"></div>
        <div class="card__setting">
          <div>
            <h2 class="card__setting-attackValue " id="attackValue"></h2>
            <img
              class="card__setting-attack"
              src="./assets/img/bluster1.png"
              alt="bluster"
              width="50px"
            />
          </div>
          <h2 class="card__setting-name" id="name"></h2>
          <div>
            <h2 class="card__setting-hpValue" id="hpValue"></h2>
            <img
              class="card__setting-hp"
              src="./assets/img/hp.png"
              alt="hp"
              width="50px"
            />
          </div>
        </div>
        <img class="card__rarity" src="" alt="rare" width="80px" />
        <p class="card__spell" id="spell">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus,
          ex?
        </p>
      </div>`
  wrapper.append(formCard.render())

  Modal.handleOpen(wrapper, 'test title')
})

// const openModalLogin = document.getElementById('signin')
const formWrapper = document.querySelector('.forms__wrapper')
const formSignIn = new FormSignIn({
  id: 'formSignIn',
  className: 'forms__wrapper--signin'
})
const formSignUp = new FormSignUp({
  id: 'formSignUp',
  className: 'forms__wrapper--signup'
})

formWrapper.append(formSignIn.render(), formSignUp.render())

// openModalLogin.addEventListener('click', () => {
//   const formSignIn = new FormSignIn({ id: 'formSignIn' })
//   Modal.handleOpen(formSignIn.render(), 'LOGIN')
// })

// const openModalSignUp = document.getElementById('signup')

// openModalSignUp.addEventListener('click', () => {
//   const formSignUp = new FormSignUp({ id: 'formSignUp' })
//   Modal.handleOpen(formSignUp.render(), 'SignUp')
// })

window.addEventListener('load', function() {
  new Modal().render()
})

// ***** TABS FORM ***** //
function handleForm() {
  const tab = document.querySelector('.forms__tab')
  const tabsTitles = document.querySelectorAll('.forms__tab--text')
  const formSignIn = document.getElementById('formSignIn')
  const formSignUp = document.getElementById('formSignUp')
  tab.addEventListener('click', function(e) {
    tabsTitles.forEach((title, index) => {
      title.classList.remove('active')
    })

    if (e.target.textContent === 'Sign Up') {
      formSignUp.style.left = '50%'
      formSignIn.style.left = '-50%'
    } else {
      formSignUp.style.left = '110%'
      formSignIn.style.left = '50%'
    }
    
    e.target.classList.add('active')
  })
}
handleForm()
