/* ЗАДАЧА - 1
 * Создать объект пользователя с его данными - имя, фамилия, дата рождения, о себе.
 * Создать по переменной для каждого из свойств объекта пользователя.
 * Выполнить задание И самым "в лоб" способом, И используя деструктуризацию
 * ПРОДВИНУТАЯ СЛОЖНОСТЬ: при помощи деструктуризации сохранить значение каждого свойства объекта
 * в переменную, имя которой отличается от названия свойства.
 * Если искомого свойства нет в объекте = по умолчанию в переменную присваивать пустую строку.
 * */

const user = {
	name: 'andy',
	lastName: 'Gigabitze',
	birthDay: '16.06.1999',
	about: 'lorem',
}

// const userName = user.name
// const userLastName = user.lastName
// const userBirth = user.birthDay

// const { name: tempVarInThisUserObject, lastName, birthDay, about } = user
// alert(tempVarInThisUserObject)