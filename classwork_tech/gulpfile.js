const gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	imagemin = require('gulp-imagemin'),
	uglify = require('gulp-uglify'),
	minifyjs = require('gulp-js-minify'),
	cleaner = require('gulp-clean'),
	cleanCSS = require('gulp-clean-css'),
	minify = require('gulp-minify'),
	sass = require('gulp-sass');

const path = {
	src: {
		html: 'src/*.html',
		scss: 'src/style/**/*.*',
		img: 'src/img/*.*',
		js: 'src/js/*.js',
		static: 'src/static/*.*',
	},
	dist: {
		html: 'dist',
		css: 'dist/css',
		img: 'dist/img',
		js: 'dist/js',
		static: 'dist/static',
	},
};

/**************** F U N C T I O N S ***************/

const htmlBuild = () =>
	gulp.src(path.src.html).pipe(gulp.dest(path.dist.html)).pipe(browserSync.stream());

const scssBuild = () =>
	gulp
		.src(path.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
		.pipe(minify())
		.pipe(gulp.dest(path.dist.css))
		.pipe(browserSync.stream());

const imgBuild = () => gulp.src(path.src.img).pipe(imagemin()).pipe(gulp.dest(path.dist.img));

const jsBuild = () =>
	gulp
		.src(path.src.js)
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(minifyjs())
		.pipe(gulp.dest(path.dist.js))
		.pipe(browserSync.stream());

const clean = () =>
	gulp
		.src(path.dist.html, { allowEmpty: true })
		.pipe(cleaner())
		.pipe(cleanCSS({ compatibility: 'ie8' }));

const staticBuild = () => gulp.src(path.src.static).pipe(gulp.dest(path.dist.static));
/****************** W A T C H E R ***************/

const watcher = () => {
	browserSync.init({
		server: {
			baseDir: './dist',
		},
	});
	gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
	gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
	gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
};

/**************** T A S K S ****************/

gulp.task(
	'default',
	gulp.series(clean, htmlBuild, scssBuild, jsBuild, imgBuild, watcher, staticBuild)
); // запускает таски по очереди
// gulp.task('default', gulp.parallel(htmlBuild, cssBuild, imgBuild, watcher)); // запускает таски паралельно друг другу
