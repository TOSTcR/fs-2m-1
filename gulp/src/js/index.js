function welcomeUser() {
	const input = document.getElementById('name').value;
	alert(`Welcome ${input}`);
}


window.addEventListener('DOMContentLoaded', () => {
	const btn = document.getElementById('btn');
	btn.addEventListener('click', welcomeUser);
});
