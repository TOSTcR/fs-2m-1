// There is some `mailStorage` - a little simulation of real storage with email letters.
const mailStorage = [
	{
		subject: 'Hello world',
		from: 'gogidoe@somemail.nothing',
		to: 'lolabola@ui.ux',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'How could you?!',
		from: 'ladyboss@somemail.nothing',
		to: 'ingeneer@nomail.here',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'Acces denied',
		from: 'info@cornhub.com',
		to: 'gogidoe@somemail.nothing',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
]

window.addEventListener('DOMContentLoaded', () => {
	const parent = document.querySelector('.emails')
	showLetter(mailStorage, parent)

	const loader = document.querySelector('.loader')

	setTimeout(() => {
		parent.style.opacity = 1
		loader.style.opacity = 0
	}, 2000)
})

function showLetter(data, container) {
	data.forEach(function (item) {
		const wrapper = document.createElement('div')
		wrapper.className = 'email-item'

		// EXAMPLE 2
		// const titleSubject = createComponent('h4', item.subject, { className: 'email-subject' })
		// const linkFrom = createComponent('a', item.from, {
		// 	className: 'email-subtext email-from',
		// 	href: item.from,
		// })
		// const linkTo = createComponent('a', item.to, {
		// 	className: 'email-subtext email-to',
		// 	href: item.to,
		// 	id: 'root-link'
		// })
		// const textLetter = createComponent('p', item.text, { className: 'email-text' })

		const titleSubject = createComponent('h4', 'email-subject', item.subject)
		const linkFrom = createComponent('a', 'email-subtext email-from', item.from, item.from)
		const linkTo = createComponent('a', 'email-subtext email-to', item.to, item.to)
		const textLetter = createComponent('p', 'email-text', item.text, true)
		const deleteLeter = createComponent('span', 'email-delete', 'x')

		const wrapperLink = document.createElement('div')
		wrapperLink.className = 'email-subtext-wrapper'

		wrapperLink.append(linkFrom, linkTo)

		wrapper.append(titleSubject, wrapperLink, textLetter, deleteLeter)

		container.append(wrapper)

		wrapper.addEventListener('click', event => {
			textLetter.hidden = !textLetter.hidden

			if (event.target.classList.contains('email-delete')) {
				event.currentTarget.remove()
			}
		})
	})
}

function createComponent(tag, classValue, content, attribute = '') {
	const element = document.createElement(tag)
	element.className = classValue
	element.textContent = content

	if (attribute !== '') {
		element.href = `mailto: ${attribute}`
	}

	if (tag === 'p') {
		element.hidden = attribute
	}

	return element
}
// EXAMPLE 2
// function createComponent(tag, content, attribute) {
// 	const element = document.createElement(tag)
// 	element.textContent = content

// 	for (let [key, value] of Object.entries(attribute)) {
// 		element[key] = value
// 	}

// 	return element
// }

/*** THE TASK IS ***
 * 1) Show up all the emails on the screen, using only JS creating elements.
 *    Every letter should have hidden text. Show text, only after the user clicks on the emails item.
 *    It is necessary to use only one event listener for container with letters.
 *
 * 2) Implement toggle text effect. It means that only one text on only letter can be showed at the same time.
 *    If user made a click on the letter, which doesn't has showed text - you need to close current opened text, and only then open the text for the letter that was just clicked.
 *
 * 3) Create "New Mail" button. After pressing this button user need to see a modal window with the form for create mew email letter.
 *    Fields for this form are: Email Title, To (email of the recipient), letter text (up to 500 symbols), "Send" button. By the way, you need automatically fill the 'from' property for each letter, and it will be "gogi@gogimail.go".
 *    If user want to close the modal window, he can do this, by clicking on the cross sign at the top right corner of the modal window.
 *    Modal window size is 500px both width and height. It shows up at the bottom right corner of the page, user can get access to any other part of functionality while modal window is showing.
 *
 * 4) Every letter needs to have a "Delete button, which will delete this particular letter both from the page and from the mailStorage.
 * */
