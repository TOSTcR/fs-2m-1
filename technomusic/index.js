// ---------- BURGER ----------

const burger = document.querySelector('.burger');
const burgerNav = document.querySelector('.burger-nav');

burger.addEventListener('click', () => {
	burger.classList.toggle('active');
	burgerNav.classList.toggle('active');
});

// ---------- TIMER ----------
function timer() {
	const dateNow = new Date().getTime();
	const dateNewYears = new Date('Dec 31, 2021').getTime();

	const distance = dateNewYears - dateNow;

	let days = Math.floor(distance / 1000 / 60 / 60 / 24);
	let hours = Math.floor((distance / 1000 / 60 / 60) % 24);
	let minutes = Math.floor((distance / 1000 / 60) % 60);
	let seconds = Math.floor((distance / 1000) % 60);

	document.querySelector('.time-day').textContent = days;
	document.querySelector('.time-hours').textContent = hours;
	document.querySelector('.time-minutes').textContent = minutes;
	document.querySelector('.time-seconds').textContent = seconds;
}

// setTimeout(() => {
// 	console.log('work');
// }, 2000)

setInterval(() => {
	timer();
}, 1000);
